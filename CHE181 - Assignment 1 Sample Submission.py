# Ricky Fan / WEEF TA / W 22 / CHE181 / Miilestone 1 
import streamlit as st
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from pathlib import Path
import schemdraw
import schemdraw.elements as elm

#streamlit config
st.set_page_config(layout="wide")
st.title("CHE 181 Ohm's Law Calculator")
st.write("This is the first assignment for CHE181, simluating Ohm's Law, which can be used to determine the voltage drop across all resistors, given the total voltage drop and resistance of each resistor.")
st.write("Click on any of the expanders below to configure the parameters of the circuit.")

# colour dictionary to direct the associated value to the image
colourCode={"0":"black strip.png", ##000000
            "1":"brown strip.png", ##964B00
            "2":"red strip.png", ##FF0000
            "3":"orange strip.png", ##FF5F1F
            "4":"yellow strip.png", ##FFFF00
            "5":"green strip.png", ##00FF00
            "6":"blue strip.png", ##0000FF
            "7":"violet strip.png", ##9F00FF
            "8":"grey strip.png", ##808080
            "9":"white strip.png"} ##FFFFFF

# creates columns for the streamlit layout
cols=st.columns(4)

# creates an input for voltage
volExpand=cols[0].expander("Voltage")
volValue=volExpand.number_input("Enter voltage value:", min_value=1)

# creates an input for the number of resistors
resExpand=cols[1].expander("Resistors")
resCount=resExpand.number_input("Enter number of resistors:", step=1, min_value=1)

resistorList=[]

# expands the resistor expander and creates the correct number of inputs for resistor values
for x in range(int(resCount)):
    resistorList.append(resExpand.number_input("Enter resistance of resistor {}:".format(x+1), step=1, min_value=0))

# performs relevant calculations
voltageList=[]
if len(resistorList)>0 and sum(resistorList)>0:

    # determines the current through the series circuit
    totalRes=sum(resistorList)
    current=volValue/totalRes

    # converts the resistor values to a numpy array for compatibility
    resistorList=np.array(resistorList)

    # continues if all resistors have a defined and usable value
    if all(resistorList>0):

        # begins drawing the circuit simultaneously using schemdraw
        with schemdraw.Drawing() as d:
            V=d.add(elm.MeterV().label("{} V".format(volValue)))

            # iterates through the resistor values to output the voltage drop across each resistor
            for x, res in enumerate(resistorList):
                voltageList.append(current*res)
                cols[2].write("The voltage drop across resistor {} is {} volts.".format(x+1, round(current*res,2)))
                temp=list(str(res))
                temp.reverse()
                numZeroes=0

                # cleans up the leading zeros to determine the colour of the final band
                for count, char in enumerate(temp):
                    if char=="0":
                        numZeroes=count+1
                    else:
                        break
                colourOrder=[]
                for _ in range(numZeroes):
                    temp.remove("0")
                temp.reverse()
                for char in temp:
                    if char==".":
                        pass
                    else:
                        colourOrder.append(colourCode[char])

                # appends the correct colour code to the ordered list
                colourOrder.append(colourCode[str(numZeroes)])

                # pulls the image path to be used in the directory name
                basePath=Path(__file__).parents[0]
                baseResistor=Image.open(str(basePath)+"\\resistor.png")

                # generates the image path for each colour
                for x, colour in enumerate(colourOrder):
                    position=(100*(x+1)//(1+len(colourOrder)) -1,0)
                    temp=Image.open(str(basePath)+"\\"+colour,"r")
                    
                    baseResistor.paste(temp,position)
                
                # draws the resistor with the corresponding resistance
                d.add(elm.Resistor().right().label("{} Ω".format(res)))
                cols[3].image(baseResistor)
        cols[2].write("The current through the system is {} amps.".format(round(current,2)))

        # completes the circuit
        d.add(elm.Line().down())
        d.add(elm.Line().left().to(V.start))
        
        # draws the voltage profile graph
        fig, ax=plt.subplots()
        ax.plot(range(1,int(resCount+1)),voltageList)
        plt.title("Voltage Profile")
        plt.xlabel("Resistor")
        plt.ylabel("Voltage Drop (V)")
        plt.xticks(range(1,int(resCount+1)), range(1,int(resCount+1)))
        st.pyplot(fig)

        # draws the circuit schematic.
        st.image(d.get_imagedata('jpg'))
